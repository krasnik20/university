﻿using Microsoft.EntityFrameworkCore;
using Model.Model;
using System.Linq;
using System.Threading.Tasks;

namespace University.Server.Controllers
{
    public class CoursesController : BaseController<Course>
    {
        public override Task Remove(Course entity)
        {
            dbctx.TimeTableRecords.RemoveRange(dbctx.TimeTableRecords.Where(r => r.Course.Id == entity.Id));
            return base.Remove(entity);
        }
    }
}
