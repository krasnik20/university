﻿using Microsoft.EntityFrameworkCore;
using Model.Model;
using System.Linq;
using System.Threading.Tasks;

namespace University.Server.Controllers
{
    public class StudentsController : BaseController<Student>
    {
        protected override IQueryable<Student> dbSet => base.dbSet.Include(s => s.Group);
        public override async Task<Student[]> GetAll()
        {
            var items = await base.GetAll();
            foreach (var item in items)
                if (item?.Group != null) item.Group.Students = null;
            return items;
        }

        public override async Task<Student> Get(int id)
        {
            var item = await base.Get(id);
            if (item?.Group != null) item.Group.Students = null;
            return item;
        }

        public override async Task Update(Student entity)
        {
            dbctx.Entry(entity.Group).State = EntityState.Unchanged;
            await base.Update(entity);
        }
    }
}
