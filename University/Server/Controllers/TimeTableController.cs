﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace University.Server.Controllers
{
    public class TimeTableController : BaseController<TimeTableRecord>
    {
        protected override IQueryable<TimeTableRecord> dbSet => base.dbSet.Include(r => r.Teacher).Include(r => r.Room).Include(r => r.Course);
        [HttpGet("GetTimeTable")]
        public async Task<List<List<TimeTableRecord>>> GetTimeTable(int groupId)
        {
            var records = dbSet.Where(r => r.Group.Id == groupId).AsEnumerable().GroupBy(r => r.DayOfWeek).Select(gr => gr.OrderBy(r => r.TimeSlot));
            var timeTable = new List<List<TimeTableRecord>>();
            foreach (var day in records)
                timeTable.Add(new List<TimeTableRecord>(day));
            return timeTable;
        }

        public override async Task Update(TimeTableRecord entity)
        {
            dbctx.Entry(entity.Group).State = EntityState.Unchanged;
            dbctx.Entry(entity.Course).State = EntityState.Unchanged;
            dbctx.Entry(entity.Room).State = EntityState.Unchanged;
            dbctx.Entry(entity.Teacher).State = EntityState.Unchanged;
            await base.Update(entity);
        }
    }
}
