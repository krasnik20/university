﻿using Microsoft.EntityFrameworkCore;
using Model.Model;
using System.Linq;
using System.Threading.Tasks;

namespace University.Server.Controllers
{
    public class TeachersController : BaseController<Teacher>
    {
        public override Task Remove(Teacher entity)
        {
            dbctx.TimeTableRecords.RemoveRange(dbctx.TimeTableRecords.Where(r => r.Teacher.Id == entity.Id));
            return base.Remove(entity);
        }
    }
}
