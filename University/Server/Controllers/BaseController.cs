﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Model.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace University.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseController<T> : ControllerBase where T : class, IEntity
    {
        protected virtual DatabaseContext dbctx { get; set; } = new DatabaseContext();
        protected virtual IQueryable<T> dbSet => dbctx.Set<T>();

        [HttpGet("GetOne")]
        public virtual async Task<T> Get(int id)
        {
            return await dbSet.FirstOrDefaultAsync(s => s.Id == id);
        }

        [HttpGet("GetAll")]
        public virtual async Task<T[]> GetAll() => await dbSet.ToArrayAsync();

        [HttpPost("Add")]
        public virtual async Task Add(T entity)
        {
            dbctx.Attach(entity);
            await dbctx.SaveChangesAsync();
        }

        [HttpPost("Remove")]
        public virtual async Task Remove(T entity)
        {
            dbctx.Remove(entity);
            await dbctx.SaveChangesAsync(); 
        }

        [HttpPost("Update")]
        public virtual async Task Update(T entity)
        {
            dbctx.Update(entity);
            await dbctx.SaveChangesAsync();
        }
    }
}
