﻿using Microsoft.EntityFrameworkCore;
using Model.Model;
using System.Linq;
using System.Threading.Tasks;

namespace University.Server.Controllers
{
    public class GroupsController : BaseController<Group>
    {
        public override async Task<Group[]> GetAll()
        {
            var items = await base.GetAll();
            foreach(var item in items)
                item.Students = null;
            return items;
        }

        public override async Task<Group> Get(int id)
        {
            var item = await base.Get(id);
            if (item != null)
            {
                await dbctx.Entry(item).Collection(g => g.Students).LoadAsync();
                foreach (var student in item.Students)
                    student.Group = null;
            }
            return item;
        }

        public override async Task Update(Group entity)
        {
            entity.Students = null;
            await base.Update(entity);
        }

        public override Task Add(Group entity)
        {
            entity.Students = null;
            return base.Add(entity);
        }

        public override Task Remove(Group entity)
        {
            dbctx.TimeTableRecords.RemoveRange(dbctx.TimeTableRecords.Where(r => r.Group.Id == entity.Id));
            return base.Remove(entity);
        }
    }
}
