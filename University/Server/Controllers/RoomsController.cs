﻿using Microsoft.EntityFrameworkCore;
using Model.Model;
using System.Linq;
using System.Threading.Tasks;

namespace University.Server.Controllers
{
    public class RoomsController : BaseController<Room>
    {
        public override Task Remove(Room entity)
        {
            dbctx.TimeTableRecords.RemoveRange(dbctx.TimeTableRecords.Where(r => r.Room.Id == entity.Id));
            return base.Remove(entity);
        }
    }
}
