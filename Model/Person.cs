﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Model
{
    public class Person : IEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Email { get; set; }

        [NotMapped]
        public string FullName { get => $"{LastName} {FirstName} {Patronymic}"; }
        
        public override string ToString() => FullName;

        [NotMapped]
        public virtual bool IsComplete { get => !string.IsNullOrWhiteSpace(FirstName) && !string.IsNullOrWhiteSpace(LastName); }
    }
}
