﻿using System.Collections.Generic;

namespace Model.Model
{
    public class Teacher : Person
    {
        public string Degree { get; set; }
        public string Description { get; set; }
        public override bool IsComplete { get => base.IsComplete && !string.IsNullOrWhiteSpace(Degree); }
    }
}
