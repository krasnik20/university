﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Model
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
