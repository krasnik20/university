﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Model
{
    public class TimeTableRecord : IEntity
    {
        public int Id { get; set; }
        public Teacher Teacher { get; set; }
        public Room Room { get; set; }
        public Group Group { get; set; }
        public Course Course { get; set; }
        public int TimeSlot { get; set; }
        public int DayOfWeek { get; set; }
        [NotMapped]
        public bool IsComplete => Teacher != null && Room != null && Course != null && Group != null;
    }
}
