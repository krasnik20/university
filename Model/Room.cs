﻿namespace Model.Model
{
    public class Room : IEntity
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public override string ToString() => Number;
    }
}
