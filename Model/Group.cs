﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Model
{
    public class Group : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Degree Degree { get; set; }
        public int EducationDuration { get; set; }
        public int CurrentYear { get; set; }
        public List<Student> Students { get; set; }
        public override string ToString() => Name;
        public bool IsComplete { get => !string.IsNullOrWhiteSpace(Name) && EducationDuration != 0 && CurrentYear != 0; }
    }

    public enum Degree
    {
        Bachelor,
        Master,
        Gratuate,
        Specialist
    }

    public static class DegreeExtentions
    {
        public static string GetName(this Degree degree)
        {
            return degree switch
            {
                Degree.Bachelor => "Бакалавриат",
                Degree.Master => "Магистратура",
                Degree.Gratuate => "Аспирантура",
                Degree.Specialist => "Специалитет"
            };
        }
    }
}
