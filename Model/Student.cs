﻿namespace Model.Model
{
    public class Student : Person
    {
        public Group Group { get; set; }
        public override bool IsComplete { get => base.IsComplete && Group != null;}
}
}
