﻿namespace Model.Model
{
    public class Course : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public override string ToString() => Name;
    }
}
